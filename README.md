# 环境要求:
    python 3.6+ 版本
    mysql 5.6+

# 使用方法：

1. **python3 mysql redis 环境安装(略)**

2. **参考conf/db.conf.demo 配置自己的环境信息**。

3. **python3 虚拟环境安装**
      ```
        #进入项目根目录
        python3 -m venv env
        source env/bin/activate
        cd install && pip install -r pip_list.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com
        cd ../cmdb
        #创建超级用户
        python manage.py createsuperuser 
        #如果想修改密码
        python manage.py changepassword
        #创建表结构
        python manage.py makemigrations
        python manage.py migrate
        #测试启动程序
        python manage.py runserver 0.0.0.0:12000
        #启动成功后,可以开启另外一个终端,运行自愈系统,注意进入虚拟环境
        cd butterfly && python AutoPlay.py
        #butterfly 启动成功后启动,启动celery,注意进入虚拟环境
        cd butterfly && celery worker -A tasks -c 4 --loglevel=info
        #以上命令完成后,则配置和启动完毕。
               
      ```

4. **故障自愈配置**
      ```
        0> 系统管理配置-配置邮件服务器信息和API权限验证
        1> 首先配置资产用户-连接远程主机的账号密码或者密钥
        2> 配置资产项目-自愈的白名单
        3> 配置故障自愈-报警后出发的执行动作
        4> 配置联系人和联系组-告警邮件发送
        4> 配置监控项-你监控的目标和要自愈动作的逻辑配置
      ```
      

5. **web展现**
    
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/user.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/asset.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/asset_user.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/auto_recovery.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/monitor_config.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/alert_history.jpg)
   




