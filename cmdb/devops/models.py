from django.db import models

# 文件更新记录
#创建表方法
# python manage.py makemigrations
# python manage.py migrate

class MonitorConfig(models.Model):
	name = models.CharField(max_length=200,blank=True, null=True,unique=True,verbose_name="任务名称")
	type_list = [
		(0, '站点检测'),
		(1, '端口检测'),
		#(2, 'ping检测'),
		#(3, '进程检测'),
		#(4, '自定义脚本检测')
		]
	type = models.IntegerField(choices=type_list, blank=True, null=True, verbose_name="类型")
	address = models.CharField(max_length=200, blank=True, null=True,verbose_name="IP或者域名")
	port = models.IntegerField(blank=True, null=True, verbose_name="端口")
	url = models.CharField(max_length=200, blank=True, null=True, verbose_name="地址")
	frequency_list = [
		(0, '1分钟'),
		(1, '5分钟'),
		(2, '15分钟'),
		(3, '30分钟'),
		(4, '60分钟')]
	frequency = models.IntegerField(choices=frequency_list, blank=True, null=True, verbose_name="频率")

	status = models.BooleanField( verbose_name="状态")
	alert_status_list = [
		(1, '报警状态'),
		(2, '故障恢复')]
	alert_status = models.IntegerField(choices=alert_status_list, blank=True, null=True, verbose_name="报警态")
	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="更新时间")
	remarks = models.CharField(max_length=500, blank=True, null=True, verbose_name="备注")
	auto_status = models.BooleanField(default=True,verbose_name="api请求状态")

	alert_type_list = [
		(1, '邮件'),
		(2, '钉钉')]
	alert_type = models.IntegerField(choices=alert_type_list, blank=True, null=True, verbose_name="告警方式")

	class Meta:
		ordering = ['-utime']

class AutoReCovery(models.Model):
	item = models.CharField(max_length=80,blank=True, null=True,unique=True,verbose_name="自愈项名称")
	alertime = models.IntegerField(blank=True, null=True,verbose_name="告警alerttime次后开始自愈动作")
	fixtime = models.IntegerField(blank=True, null=True,verbose_name="告警后到恢复前自愈动作执行fixtime次后终止动作")
	action = models.CharField(max_length=2000, blank=True, null=True, verbose_name="自愈动作")
	status = models.BooleanField(default=True,verbose_name="自愈状态")
	mc = models.ManyToManyField(MonitorConfig, blank=True, related_name='mc', verbose_name='监控项')