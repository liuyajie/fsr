# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2021-07-07 15:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0015_monitorconfig_remarks'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitorconfig',
            name='frequency',
            field=models.IntegerField(blank=True, choices=[(0, '1分钟'), (1, '5分钟'), (2, '15分钟'), (3, '30分钟'), (4, '60分钟')], null=True, verbose_name='频率'),
        ),
        migrations.AlterField(
            model_name='monitorconfig',
            name='type',
            field=models.IntegerField(blank=True, choices=[(0, '站点检测'), (1, '端口检测'), (2, 'ping检测'), (3, '进程检测'), (4, '自定义脚本检测')], null=True, verbose_name='类型'),
        ),
    ]
