# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2021-07-07 14:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0013_autorecovery'),
    ]

    operations = [
        migrations.CreateModel(
            name='MonitorConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True, unique=True, verbose_name='任务名称')),
                ('type', models.CharField(blank=True, max_length=200, null=True, verbose_name='类型')),
                ('url', models.CharField(blank=True, max_length=200, null=True, verbose_name='地址')),
                ('frequency', models.CharField(blank=True, max_length=200, null=True, verbose_name='频率')),
                ('status', models.CharField(blank=True, max_length=200, null=True, verbose_name='状态')),
                ('utime', models.DateTimeField(auto_now=True, null=True, verbose_name='更新时间')),
            ],
            options={
                'ordering': ['-utime'],
            },
        ),
        migrations.AlterField(
            model_name='autorecovery',
            name='item',
            field=models.CharField(blank=True, max_length=80, null=True, unique=True, verbose_name='自愈项名称'),
        ),
    ]
